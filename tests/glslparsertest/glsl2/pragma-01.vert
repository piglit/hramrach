/* PASS */
#pragma optimize(on)
#pragma debug(off)

void main()
{
  gl_Position = gl_Vertex;
}

#pragma debug(on)
#pragma optimize(off)
