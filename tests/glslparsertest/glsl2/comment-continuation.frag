void main()
{
// This is a comment with a trailing backslash that shouldn't matter. \
	vec4 color = vec4(0.0, 1.0, 0.0, 0.0);
	gl_FragColor = color;
}
