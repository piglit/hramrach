/* FAIL - cannot mix void and non-void parameters
 */
float a(float, void);

void main()
{
  gl_Position = gl_Vertex;
}
